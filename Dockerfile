# Utilisez une image de base appropriée
FROM node:14

# Définissez le répertoire de travail
WORKDIR /app

# Copiez les fichiers de l'application dans le conteneur
COPY . .

# Installez les dépendances
RUN npm install

# Exposez le port sur lequel l'application écoute
EXPOSE 3000

# Commande pour démarrer l'application
CMD ["npm", "start"]
